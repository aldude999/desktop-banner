﻿
Public Class Form1
    Dim dataarray = ""
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ShellAppBar1.DockingEdge = LogicNP.ShellObjects.DockingEdges.Top
        ShellAppBar1.HostForm.Height = 15
        'Currently using ShellObjects library. Looking for alternative (AKA Free)
        If (My.Computer.FileSystem.FileExists("setup") = True) Then
        Else
            MessageBox.Show("Since this is your first time running this application, lets do some setup!")
            Installer1.ShowDialog()
        End If



        Timer1.Start()
        Timer1.Interval = My.Settings.READTIME * 60000 ' 60000 is 1 minute
        TrueTick() ' Lets do a workaround to not being able to call Timer1_Tick directly >.>

    End Sub



    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        TrueTick()
    End Sub
    Private Sub TrueTick()
        If "T" = My.Settings.TYPE Then
            Dim readfile = My.Computer.FileSystem.ReadAllText(My.Settings.LOCATION)
            dataarray = Split(readfile, vbCrLf)
        ElseIf "G" = My.Settings.TYPE Then
        ElseIf "S" = My.Settings.TYPE Then
            Try
                My.Computer.FileSystem.DeleteFile("cache")
            Catch ex As Exception

            End Try
            Try
                My.Computer.Network.DownloadFile(My.Settings.LOCATION, "cache")
                Dim readfile = My.Computer.FileSystem.ReadAllText("cache")
                dataarray = Split(readfile, vbCrLf)
            Catch ex As Exception
                My.Computer.FileSystem.WriteAllText("error.log", DateAndTime.Now & ": Failed to retreive Information!" & vbCrLf, 1)
            End Try


        End If
        Try
            If dataarray(1).substring(0, 1) = Chr(34) Then
                lblCenter.Text = dataarray(1).trim(Chr(34))
                ShellAppBar1.HostForm.BackColor = System.Drawing.Color.FromName(dataarray(3))
                lblLeft.Text = dataarray(0)
                lblRight.Text = dataarray(2)
                lblLeft.Visible = True
                lblRight.Visible = True
            End If
        Catch ex As Exception

        End Try
        'Extra functionality to allow classifications (Can be enabled by uncommenting)
        ' Try
        'If dataarray(1) = "U" Then
        'lblCenter.Text = "Unclassified"
        'ShellAppBar1.HostForm.BackColor = Color.Green
        'ElseIf dataarray(1) = "S" Then
        'lblCenter.Text = "Secret"
        'ShellAppBar1.HostForm.BackColor = Color.Red
        'ElseIf dataarray(1) = "TS" Then
        'lblCenter.Text = "Top Secret"
        'ShellAppBar1.HostForm.BackColor = Color.Orange
        'ElseIf dataarray(1) = "SCI" Then
        'lblCenter.Text = "SCI"
        'ShellAppBar1.HostForm.BackColor = Color.Yellow
        'End If
        ' Catch ex As Exception

        '  End Try
        If My.Settings.NetCheck = True Then 'Going to improve this
            If My.Computer.Network.IsAvailable = False Then
                ShellAppBar1.HostForm.BackColor = Color.Yellow
                lblLeft.Text = "Network Status: Issue Detected!"
                lblLeft.Visible = True
            Else
                lblLeft.Text = "Network Status: OK"
                lblLeft.Visible = True
            End If
        End If
    End Sub
End Class
