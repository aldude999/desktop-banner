﻿Imports System.Windows.Forms
Imports System.IO

Public Class Installer1
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If RadioButton2.Checked = True Then
            My.Settings.TYPE = "T"
            My.Settings.LOCATION = Label2.Text
        ElseIf RadioButton3.Checked = True Then
            My.Settings.TYPE = "G"
        ElseIf RadioButton1.Checked = True Then
            My.Settings.TYPE = "S"
            My.Settings.LOCATION = TextBox1.Text
        Else
            MessageBox.Show("Please choose a setting!")
            End
        End If
        If CheckBox1.Checked = True Then
            My.Settings.NetCheck = True
        Else
            My.Settings.NetCheck = False
        End If
        My.Settings.READTIME = NumericUpDown1.Value
        My.Computer.FileSystem.WriteAllText("setup", "1", 0)
        My.Settings.Save()
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If RadioButton1.Checked = True Then
            TextBox1.Enabled = True

        Else
            TextBox1.Enabled = False

        End If
    End Sub


    Private Sub RadioButton2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If RadioButton2.Checked = True Then
            Button1.Enabled = True
        Else
            Button1.Enabled = False
        End If
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
    Dim openFileDialog1 As OpenFileDialog = New OpenFileDialog

        ' Set filter options and filter index.
        openFileDialog1.Filter = "Text Files (*.txt)|*.txt|XML Files (*.xml)|*.xml|All Files (*.*)|*.*"
        openFileDialog1.FilterIndex = 1

        openFileDialog1.Multiselect = True

        ' Call the ShowDialog method to show the dialogbox.
        Dim UserClickedOK As Boolean = openFileDialog1.ShowDialog
        Label2.Text = openFileDialog1.FileName
    End Sub
End Class
