﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblLeft = New System.Windows.Forms.Label()
        Me.lblCenter = New System.Windows.Forms.Label()
        Me.lblRight = New System.Windows.Forms.Label()
        Me.ShellAppBar1 = New LogicNP.ShellObjects.ShellAppBar(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.ShellAppBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblLeft
        '
        Me.lblLeft.AutoSize = True
        Me.lblLeft.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLeft.Location = New System.Drawing.Point(3, 0)
        Me.lblLeft.Name = "lblLeft"
        Me.lblLeft.Size = New System.Drawing.Size(42, 13)
        Me.lblLeft.TabIndex = 1
        Me.lblLeft.Text = "lblLeft"
        Me.lblLeft.Visible = False
        '
        'lblCenter
        '
        Me.lblCenter.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblCenter.AutoSize = True
        Me.lblCenter.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCenter.Location = New System.Drawing.Point(187, 0)
        Me.lblCenter.Name = "lblCenter"
        Me.lblCenter.Size = New System.Drawing.Size(102, 13)
        Me.lblCenter.TabIndex = 2
        Me.lblCenter.Text = "No Configuration"
        '
        'lblRight
        '
        Me.lblRight.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRight.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRight.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblRight.Location = New System.Drawing.Point(361, 0)
        Me.lblRight.Name = "lblRight"
        Me.lblRight.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.lblRight.Size = New System.Drawing.Size(245, 13)
        Me.lblRight.TabIndex = 3
        Me.lblRight.Text = "lblRight"
        Me.lblRight.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblRight.Visible = False
        Me.ShellAppBar1.HostForm = Me
        '
        'Timer1
        '
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(607, 15)
        Me.Controls.Add(Me.lblRight)
        Me.Controls.Add(Me.lblCenter)
        Me.Controls.Add(Me.lblLeft)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.ShellAppBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents lblLeft As System.Windows.Forms.Label
    Private WithEvents lblCenter As System.Windows.Forms.Label
    Private WithEvents lblRight As System.Windows.Forms.Label
    Friend WithEvents ShellAppBar1 As LogicNP.ShellObjects.ShellAppBar
    Friend WithEvents Timer1 As System.Windows.Forms.Timer

End Class
