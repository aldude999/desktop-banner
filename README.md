#### README ####

### TODO ###
Remove ShellObject library and find an alternative

Add Group Policy settings

Create Installer

Fix bugs where timer stops ticking

### Use ###
This application allows an administrator to display a banner acrossed the top of the screen displaying any of 3 messages in the left, center, and right of the banner.

The application can gather data from a text file, Group Policy (not implimented yet), or an FTP/HTTP/S server in the form of a DOS formatted text file.
### Installation ###
Installer to come later, but for now, it can be installed by creating a shortcut to the application in the startup folder for all users.

On first run, the application will ask you to set it up, giving you the option to choose a file (from the local disk or a network resource), group policy, or from a specific server file.

It will also give you a choice to set the update interval in minutes, and to detect if all network interfaces are disconnected.

### Setting Up The Banner ###
If you elect to use a file or server to retrieve the banner information, it will need to have the following format:

LEFT FIELD TEXT

CENTER FIELD TEXT

RIGHT FIELD TEXT

BACKGROUND COLOR

The background color should be a written color, such as green, orange, red, etc.

### Extra Server Scripting ###
You can use a bash script and cron to allow the server to display relevant information, such as weather, or other relevant information.
There is an example bash script in the "Server" folder that will grab weather and update the right field as needed.
