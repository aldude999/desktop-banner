#! /bin/bash
#Script created by Harold Bramton September, 2014 to fill a certain need.
#Modify as needed to meet you needs and share with others
#
#Enter the desired alert email address below
FILEWRITE="/home/www/html/networkstatus/external"
#
#
#This is currently set to check the National Hurrican Center's ATLANTIC TROPICAL WEATHER SUMMARY
#If you want to monitor other feeds refer to http://nhc.noaa.gov/aboutrss.shtml
FEED="http://forecast.weather.gov/MapClick.php?lat=35.4771&lon=-97.5687&unit=0&lg=english&FcstType=dwml"
#
#Do not edit below these lines unless you understand exactly what it is doing...
#
#Grab today's date for later use
DATE=`date +%m%d%Y`
#
#Grab the current hour so we dont' get 2 update in same hourly period
HOUR=`date +%H`
#
#scrape the rss feed, remove the header crap, strip off the footer crap and dump output to a text file
curl -s $FEED | sed -e '1,30d' | head --lines=-13 | sed 's/<br \/>//' > /home/www/html/networkstatus/severe-weather.data
#Uncomment the line below if you wish to test the script's email capability, debug or etc.
#echo "hurricane warning" >> /var/severe-weather.data
#
#
#Check to see if we have alerted within this hour, on today's date.
if ! grep -q "$DATE" /home/www/html/networkstatus/severe-weather.alert &> /dev/null && ! grep -q "$HOUR" /home/www/html/networkstatus/severe-weather.alert &> /dev/null;
then
#Check for the inclimate weather keywords
#If keywords are found, send alert email, attaching clean feed data, and create an alerted flag
    if grep -i "hurricane watch" /home/www/html/networkstatus/severe-weather.data &> /dev/null
    then
       echo "n"
    elif grep -i "hurricane warning" /home/www/html/networkstatus/severe-weather.data &> /dev/null
    then
        #mail -s "HURRICANE WARNING alert from NOAA" $EMAIL < /var/severe-weather.data
        #echo "HURRICANE alert sent $TIME $DATE" > /var/severe-weather.alert
        echo "n"
    elif grep -i "tornado watch" /home/www/html/networkstatus/severe-weather.data &> /dev/null
    then
        #mail -s "TORNADO WATCH alert from NOAA" $EMAIL < /var/severe-weather.data
        #echo "HURRICANE alert sent $TIME $DATE" > /var/severe-weather.alert
        echo -e "WATCH\n\nWeather Information: TORNADO WATCH\nyellow" > $FILEWRITE
        sed -i '3s/.*/Weather Information: TORNADO WATCH /' /home/www/html/networkstatus/SETTINGS.txt
        unix2dos /home/www/html/networkstatus/SETTINGS.txt /home/www/html/networkstatus/SETTINGS.txt &> /dev/null
    elif grep -i "tornado warning" /home/www/html/networkstatus/severe-weather.data &> /dev/null
    then
        #mail -s "TORNADO WARNING alert from NOAA" $EMAIL < /var/severe-weather.data
        #echo "HURRICANE alert sent $TIME $DATE" > /var/severe-weather.alert
        echo -e "WARNING\n\nWeather Information: TORNADO WARNING\nred" > $FILEWRITE
        sed -i '3s/.*/Weather Information: TORNADO WARNING /' /home/www/html/networkstatus/SETTINGS.txt
        unix2dos /home/www/html/networkstatus/SETTINGS.txt /home/www/html/networkstatus/SETTINGS.txt &> /dev/null

    elif grep "tropical depression" /home/www/html/networkstatus/severe-weather.data &> /dev/null
    then
        #mail -s "TROPICAL DEPRESSSION alert from NOAA" $EMAIL < /var/severe-weather.data
        #echo "HURRICANE alert sent $TIME $DATE" > /var/severe-weather.alert
        echo "n"
    elif grep "flood" /home/www/html/networkstatus/severe-weather.data &> /dev/null
    then
        #mail -s "FLOOD alert from NOAA" $EMAIL < /var/severe-weather.data
        #echo "HURRICANE alert sent $TIME $DATE" > /var/severe-weather.alert
        echo "n"
    elif grep "severe" /home/www/html/networkstatus/severe-weather.data &> /dev/null
    then
        #mail -s "severe alert from NOAA" $EMAIL < /var/severe-weather.data
        #echo "HURRICANE alert sent $TIME $DATE" > /var/severe-weather.alert
        echo -e "WATCH\n\nWeather Information: SERVERE WEATHER\ngreen" > $FILEWRITE
        sed -i '3s/.*/Weather Information: SERVERE WEATHER /' /home/www/html/networkstatus/SETTINGS.txt
        unix2dos /home/www/html/networkstatus/SETTINGS.txt /home/www/html/networkstatus/SETTINGS.txt &> /dev/null

    else
        echo -e "\n\nWeather Information: Normal\ngreen" > $FILEWRITE
        sed -i '3s/.*/Weather Information: Normal /' /home/www/html/networkstatus/SETTINGS.txt
        unix2dos /home/www/html/networkstatus/SETTINGS.txt /home/www/html/networkstatus/SETTINGS.txt &>/dev/null


    fi
else

#There's no keyword hits, so lets make sure the alert flag is no longer set...
    echo -e "\n\nWeather Information: Normal\ngreen" > $FILEWRITE
    sed -i '3s/.*/Weather Information: Normal /' /home/www/html/networkstatus/SETTINGS.txt
    unix2dos /home/www/html/networkstatus/SETTINGS.txt /home/www/html/networkstatus/SETTINGS.txt &>/dev/null

fi
